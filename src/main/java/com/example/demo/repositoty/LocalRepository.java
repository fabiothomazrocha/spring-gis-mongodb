package com.example.demo.repositoty;

import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.entity.Local;

public interface LocalRepository extends MongoRepository<Local, String>{
	
	  List<Local> findByNomeAndLocalizacaoNear(String nome, Point p, Distance d);


}
