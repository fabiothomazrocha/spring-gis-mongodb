package com.example.demo.entity;

import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "locais")
public class Local {
	private String id;
	  private String nome;
	  private String descricao;
	  private GeoJsonPoint localizacao;
	public Local( String nome, String descricao, GeoJsonPoint location) {
		super();

		this.nome = nome;
		this.descricao = descricao;
		this.localizacao = location;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public GeoJsonPoint getLocation() {
		return localizacao;
	}
	public void setLocation(GeoJsonPoint location) {
		this.localizacao = location;
	}
	@Override
	public String toString() {
		return "Local [id=" + id + ", nome=" + nome + ", descricao=" + descricao + ", location=" + localizacao + "]";
	}
	
	  
	
	  

}
