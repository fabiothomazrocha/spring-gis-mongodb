package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PontoDTO;
import com.example.demo.entity.Local;
import com.example.demo.repositoty.LocalRepository;

@CrossOrigin
@RestController
public class LocalController {
	@Autowired
	private LocalRepository repository;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	public final List<Local> getLocations(@RequestParam("lat") String latitude, @RequestParam("long") String longitude,
			@RequestParam("distancia") double distancia, @RequestParam(value = "nome", required = false) String nome) {

		return this.repository.findByNomeAndLocalizacaoNear(nome,
				new Point(Double.valueOf(longitude), Double.valueOf(latitude)),
				new Distance(distancia, Metrics.KILOMETERS));
	}

	@CrossOrigin
	@RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public  ResponseEntity<?>  addLocations(@RequestBody PontoDTO ponto) {

		List<Local> locais = new ArrayList<>();

		final GeoJsonPoint locationPoint = new GeoJsonPoint(Double.valueOf(ponto.getLongitude()),
				Double.valueOf(ponto.getLatitude()));

		locais.add(new Local(ponto.getLocal(), ponto.getTipo(), locationPoint));

		this.repository.saveAll(locais);
		Map<Object, String> map = new HashMap<Object,String>();
		map.put("menssagem", "Ocorrencia criada com sucesso!");
		 return new ResponseEntity(map,HttpStatus.OK);
	}
}
