package com.example.demo.dto;

import java.io.Serializable;
import java.util.Objects;

public class PontoDTO implements Serializable{
	private String local;
	private String tipo;
	private String longitude;
	  private String latitude;
	  
	  
	  public String getLongitude() {
		    return this.longitude;
		  }

		  public void setLongitude(final String longitude) {
		    this.longitude = longitude;
		  }

		  public String getLatitude() {
		    return this.latitude;
		  }

		  public void setLatitude(final String latitude) {
		    this.latitude = latitude;
		  }

		  public String getLocal() {
			return local;
		}

		public void setLocal(String local) {
			this.local = local;
		}

		public String getTipo() {
			return tipo;
		}

		public void setTipo(String tipo) {
			this.tipo = tipo;
		}

		@Override
		  public boolean equals(final Object o) {
		    if (this == o) return true;
		    if (o == null || this.getClass() != o.getClass()) return false;
		    final PontoDTO that = (PontoDTO) o;
		    return Objects.equals(this.getLongitude(), that.getLongitude()) &&
		      Objects.equals(this.getLatitude(), that.getLatitude());
		  }

		  @Override
		  public int hashCode() {
		    return Objects.hash(this.getLongitude(), this.getLatitude());
		  }
}
